package com.kolsuma.tether.gphoto2;

import static com.kolsuma.tether.gphoto2.PortResult.*;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;

class PortResultTest {

    @Test
    void okResult() {
        assertThat(PortResult.get(0), is(GP_OK));
    }

    @Test
    void genericErrorResult() {
        assertThat(PortResult.get(-1), is(GP_ERROR));
    }

    @Test
    void cameraErrorResult() {
        assertThat(PortResult.get(-113), is(GP_ERROR_CAMERA_ERROR));
    }
}
