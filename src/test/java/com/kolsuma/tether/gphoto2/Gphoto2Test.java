package com.kolsuma.tether.gphoto2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Gphoto2Test {

    @Test
    void okErrorCode() {
        assertDoesNotThrow(() -> {
            Gphoto2.check(0);
        });
    }

    @Test
    void unsuccessfulErrorCode() {
        assertThrows(GphotoException.class, () -> {
            Gphoto2.check(-1);
        });
    }
}
