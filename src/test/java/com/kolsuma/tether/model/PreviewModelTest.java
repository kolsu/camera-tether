package com.kolsuma.tether.model;

import static com.kolsuma.tether.model.ImageScaling.ACTUAL_SIZE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PreviewModelTest {

    private static PreviewModel model;
    private static ListenerStub listener;

    @BeforeEach
    void resetListener() {
        model = new PreviewModel();
        listener = new ListenerStub();
        model.addListener(listener);
    }

    @Test
    void rotationAngleChangeEventFiring() {
        model.rotateImage();

        assertTrue(listener.encounteredEvent);
    }

    @Test
    void rotateImage() {
        model.rotateImage();

        assertThat(model.getRotationAngleDegrees(), is(90));
    }

    @Test
    void rotateImageFull360() {
        model.rotateImage();
        model.rotateImage();
        model.rotateImage();
        model.rotateImage();

        assertThat(model.getRotationAngleDegrees(), is(0));
    }

    @Test
    void imageChangeEventFiring() {
        model.setImage(new byte[0]);

        assertTrue(listener.encounteredEvent);
    }

    @Test
    void imageScaleChangeEventFiring() {
        model.setImageScaling(ACTUAL_SIZE);

        assertTrue(listener.encounteredEvent);
    }
}
