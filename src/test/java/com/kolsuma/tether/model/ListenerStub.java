package com.kolsuma.tether.model;

import com.kolsuma.tether.util.function.Function;

public class ListenerStub implements Function {

    boolean encounteredEvent = false;

    @Override
    public void apply() {
        encounteredEvent = true;
    }
}
