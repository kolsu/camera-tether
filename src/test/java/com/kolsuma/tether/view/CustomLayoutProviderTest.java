package com.kolsuma.tether.view;

import static com.kolsuma.tether.view.CustomLayoutProvider.customLayoutProvider;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.awt.*;

class CustomLayoutProviderTest {

    private boolean layoutFunctionCalled = false;

    @Test
    void callProvidedLayoutFunction() {
        LayoutManager layoutManager = customLayoutProvider(() -> layoutFunctionCalled = true);

        layoutManager.layoutContainer(null);

        assertTrue(layoutFunctionCalled);
    }
}
