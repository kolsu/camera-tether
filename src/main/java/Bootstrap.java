import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

import com.kolsuma.tether.gphoto2.CameraTether;
import com.kolsuma.tether.model.PreviewModel;
import com.kolsuma.tether.view.ApplicationFrame;
import com.kolsuma.tether.view.LiveViewPanel;
import com.kolsuma.tether.view.PreviewToolbar;

import javax.swing.*;

public class Bootstrap {

    public static void main(String[] args) {
        PreviewModel previewModel = new PreviewModel();

        SwingUtilities.invokeLater(() -> {
            LiveViewPanel previewPanel = new LiveViewPanel(previewModel);
            PreviewToolbar previewToolbar = new PreviewToolbar(previewModel);
            ApplicationFrame frame = new ApplicationFrame(previewPanel, previewToolbar);

            frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
            frame.setLocation(100, 100);
            frame.setSize(1200, 800);
            frame.setVisible(true);
        });

        CameraTether tether = new CameraTether();
        tether.startLiveView(previewModel::setImage);
    }
}
