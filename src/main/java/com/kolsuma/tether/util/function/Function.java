package com.kolsuma.tether.util.function;

public interface Function {

    void apply();
}
