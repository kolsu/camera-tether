package com.kolsuma.tether.view;

import static com.kolsuma.tether.view.CustomLayoutProvider.customLayoutProvider;

import com.kolsuma.tether.model.PreviewModel;

import javax.swing.*;
import java.awt.*;

public class PreviewToolbar extends JPanel {

    private static final int toolbarSize = 25;
    private static final int toolbarButtonWidth = toolbarSize;
    private static final int toolbarButtonHeight = toolbarSize;

    public PreviewToolbar(PreviewModel previewModel) {
        setBackground(new Color(29, 30, 32));
        setPreferredSize(new Dimension(toolbarSize, 0));

        add(new LiveViewScalingButton(previewModel));
        add(new LiveViewRotationButton(previewModel));

        setLayout(customLayoutProvider(this::layoutComponents));
    }

    private void layoutComponents() {
        Component button0 = getComponent(0);
        Component button1 = getComponent(1);
        button0.setBounds(0, 0, toolbarButtonWidth, toolbarButtonHeight);
        button1.setBounds(0, toolbarButtonHeight, toolbarButtonWidth, toolbarButtonHeight);
    }
}
