package com.kolsuma.tether.view;

import static com.kolsuma.tether.model.ImageScaling.ACTUAL_SIZE;
import static java.lang.Math.toRadians;

import com.kolsuma.tether.model.ImageScaling;
import com.kolsuma.tether.model.PreviewModel;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.resizers.configurations.Rendering;

import javax.swing.*;
import java.awt.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;

public class LiveViewPanel extends JPanel {

    private PreviewModel model;

    public LiveViewPanel(PreviewModel model) {
        super(null, false);
        this.model = model;

        setBackground(new Color(29, 30, 32));

        model.addListener(this::repaint);
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        Graphics2D g = (Graphics2D) graphics;
        super.paintComponent(g);

        int panelWidth  = getWidth();
        int panelHeight = getHeight();
        byte[] imageBytes         = model.getImage();
        ImageScaling imageScaling = model.getImageScaling();
        int rotationAngleDegrees  = model.getRotationAngleDegrees();

        if (imageBytes == null) {
            return;
        }

        Image image;

        int imageResizeWidth  = panelWidth;
        int imageResizeHeight = panelHeight;

        if (rotationAngleDegrees == 90 || rotationAngleDegrees == 270) {
            imageResizeWidth = panelHeight;
            imageResizeHeight = panelWidth;
        }

        if (imageScaling == ACTUAL_SIZE) {
            image = new ImageIcon(imageBytes).getImage();
        } else {
            try {
                image = Thumbnails.of(new ByteArrayInputStream(imageBytes))
                                  .size(imageResizeWidth, imageResizeHeight)
                                  .keepAspectRatio(true)
                                  .rendering(Rendering.QUALITY)
                                  .asBufferedImage();
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }



        int imageWidth = image.getWidth(null);
        int imageHeight = image.getHeight(null);

        int x = (panelWidth - imageWidth) / 2;
        int y = (panelHeight - imageHeight) / 2;

        switch (rotationAngleDegrees) {
            case 90:
                x = (panelWidth + imageHeight) / 2;
                y = (panelHeight - imageWidth) / 2;
                break;
            case 180:
                x = (panelWidth + imageWidth) / 2;
                y = (panelHeight + imageHeight) / 2;
                break;
            case 270:
                x = (panelWidth - imageHeight) / 2;
                y = (panelHeight + imageWidth) / 2;
                break;
        }


        g.translate(x, y);
        g.rotate(toRadians(rotationAngleDegrees));
        g.drawImage(image, 0, 0, null);
    }
}
