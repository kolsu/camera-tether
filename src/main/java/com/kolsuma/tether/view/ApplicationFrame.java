package com.kolsuma.tether.view;

import static com.kolsuma.tether.view.CustomLayoutProvider.customLayoutProvider;

import javax.swing.*;
import java.awt.*;

public class ApplicationFrame extends JFrame {

    private LiveViewPanel liveViewPanel;
    private PreviewToolbar previewToolbar;

    public ApplicationFrame(LiveViewPanel liveViewPanel, PreviewToolbar previewToolbar) {
        super("Camera Tether");

        this.liveViewPanel = liveViewPanel;
        this.previewToolbar = previewToolbar;

        setDefaultCloseOperation(EXIT_ON_CLOSE);

        Container contentPane = getContentPane();
        contentPane.setBackground(new Color(47, 48, 50));
        contentPane.setLayout(customLayoutProvider(this::layoutComponents));

        contentPane.add(liveViewPanel);
        contentPane.add(previewToolbar);
    }

    private void layoutComponents() {
        Container contentPane = getContentPane();
        Dimension size = contentPane.getSize();

        int toolbarWidth = previewToolbar.getPreferredSize().width;

        int padding = 10;
        int x = padding;
        int y = padding;
        int w = size.width - x - padding - toolbarWidth - padding;
        int h = size.height - padding - padding;

        liveViewPanel.setBounds(x, y, w, h);
        previewToolbar.setBounds(w + padding*2, padding, toolbarWidth, h);
    }
}
