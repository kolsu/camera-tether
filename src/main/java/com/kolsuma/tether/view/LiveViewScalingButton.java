package com.kolsuma.tether.view;

import static com.kolsuma.tether.model.ImageScaling.ACTUAL_SIZE;
import static com.kolsuma.tether.model.ImageScaling.ZOOM_TO_FIT;

import com.kolsuma.tether.model.ImageScaling;
import com.kolsuma.tether.model.PreviewModel;

import javax.swing.*;

class LiveViewScalingButton extends JButton {

    private PreviewModel previewModel;

    LiveViewScalingButton(PreviewModel previewModel) {
        super("S");
        this.previewModel = previewModel;

        addActionListener(event -> handleButtonEvent());
    }

    private void handleButtonEvent() {
        ImageScaling imageScale = previewModel.getImageScaling();
        if (imageScale == ZOOM_TO_FIT) {
            previewModel.setImageScaling(ACTUAL_SIZE);
        } else {
            previewModel.setImageScaling(ZOOM_TO_FIT);
        }
    }
}
