package com.kolsuma.tether.view;

import com.kolsuma.tether.model.PreviewModel;

import javax.swing.*;

class LiveViewRotationButton extends JButton {

    private PreviewModel previewModel;

    LiveViewRotationButton(PreviewModel previewModel) {
        super("R");
        this.previewModel = previewModel;

        addActionListener(event -> handleButtonEvent());
    }

    private void handleButtonEvent() {
        previewModel.rotateImage();
    }
}
