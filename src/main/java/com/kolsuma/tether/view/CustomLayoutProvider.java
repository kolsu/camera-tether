package com.kolsuma.tether.view;

import java.awt.*;

public class CustomLayoutProvider implements LayoutManager {

    private Runnable function;

    static LayoutManager customLayoutProvider(Runnable function) {
        return new CustomLayoutProvider(function);
    }

    private CustomLayoutProvider(Runnable function) {
        this.function = function;
    }

    @Override
    public void addLayoutComponent(String name, Component comp) {

    }

    @Override
    public void removeLayoutComponent(Component comp) {

    }

    @Override
    public Dimension preferredLayoutSize(Container parent) {
        return null;
    }

    @Override
    public Dimension minimumLayoutSize(Container parent) {
        return null;
    }

    @Override
    public void layoutContainer(Container parent) {
        function.run();
    }
}
