package com.kolsuma.tether.model;

import static com.kolsuma.tether.model.ImageScaling.ZOOM_TO_FIT;

import com.kolsuma.tether.util.function.Function;

import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public class PreviewModel {

    private volatile byte[] image;
    private volatile AtomicInteger rotationAngleDegrees = new AtomicInteger();
    private volatile ImageScaling imageScaling = ZOOM_TO_FIT;
    private CopyOnWriteArrayList<Function> listeners = new CopyOnWriteArrayList<>();

    public void rotateImage() {
        int currentAngle, newAngle;

        do {
            currentAngle = rotationAngleDegrees.get();
            newAngle = (currentAngle == 270) ? 0 : currentAngle + 90;
        } while (!rotationAngleDegrees.compareAndSet(currentAngle, newAngle));

        fireChangeEvent();
    }

    public int getRotationAngleDegrees() {
        return rotationAngleDegrees.get();
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
        fireChangeEvent();
    }

    public ImageScaling getImageScaling() {
        return imageScaling;
    }

    public void setImageScaling(ImageScaling imageScaling) {
        this.imageScaling = imageScaling;
        fireChangeEvent();
    }

    public void addListener(Function function) {
        listeners.add(function);
    }

    private void fireChangeEvent() {
        listeners.forEach(Function::apply);
    }
}
