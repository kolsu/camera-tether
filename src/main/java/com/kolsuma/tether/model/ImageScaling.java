package com.kolsuma.tether.model;

public enum ImageScaling {

    ACTUAL_SIZE,
    ZOOM_TO_FIT
}
