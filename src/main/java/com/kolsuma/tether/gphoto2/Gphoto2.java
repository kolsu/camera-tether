package com.kolsuma.tether.gphoto2;

import com.sun.jna.Pointer;
import com.sun.jna.ptr.PointerByReference;

class Gphoto2 {

    static Pointer gp_context_new() {
        return Gphoto2Native.gp_context_new();
    }

    static void gp_camera_new(PointerByReference cameraReference) {
        int errorCode = Gphoto2Native.gp_camera_new(cameraReference);
        check(errorCode);
    }

    static void gp_file_new(PointerByReference cameraFileReference) {
        int errorCode = Gphoto2Native.gp_file_new(cameraFileReference);
        check(errorCode);
    }

    static void gp_camera_capture_preview(Pointer camera, Pointer cameraFileReference, Pointer context) {
        int errorCode = Gphoto2Native.gp_camera_capture_preview(camera, cameraFileReference, context);
        check(errorCode);
    }

    static void gp_file_get_data_and_size(Pointer cameraFile, PointerByReference dataReference, Pointer size) {
        int errorCode = Gphoto2Native.gp_file_get_data_and_size(cameraFile, dataReference, size);
        check(errorCode);
    }

    static void gp_file_free(Pointer cameraFile) {
        int errorCode = Gphoto2Native.gp_file_free(cameraFile);
        check(errorCode);
    }

    static void check(int errorCode) {
        if (errorCode != 0) {
            throw new GphotoException(PortResult.get(errorCode));
        }
    }
}
