package com.kolsuma.tether.gphoto2;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.PointerByReference;

public class Gphoto2Native {

    static {
        Native.register("gphoto2");
    }

    public static native Pointer gp_context_new();

    public static native int gp_camera_new(PointerByReference cameraReference);
    public static native int gp_file_new(PointerByReference cameraFileReference);
    public static native int gp_camera_capture_preview(Pointer camera, Pointer cameraFileReference, Pointer context);
    public static native int gp_file_get_data_and_size(Pointer cameraFile, PointerByReference dataReference, Pointer size);
    public static native int gp_file_free(Pointer cameraFile);
}
