package com.kolsuma.tether.gphoto2;

import static com.kolsuma.tether.gphoto2.Gphoto2.*;

import com.sun.jna.Pointer;
import com.sun.jna.ptr.NativeLongByReference;
import com.sun.jna.ptr.PointerByReference;

import java.util.function.Consumer;

public class CameraTether {

    private final Pointer context;

    public CameraTether() {
        context = gp_context_new();
        if (context == null) {
            throw new IllegalStateException("Failed to get GP context");
        }
    }

    public void startLiveView(Consumer<byte[]> consumer) {
        PointerByReference cameraReference = new PointerByReference();
        PointerByReference cameraFileReference = new PointerByReference();
        PointerByReference data = new PointerByReference();
        NativeLongByReference size = new NativeLongByReference();

        gp_camera_new(cameraReference);

        Pointer camera = cameraReference.getValue();

        while (true) {
            gp_file_new(cameraFileReference);
            Pointer cameraFile = cameraFileReference.getValue();
            gp_camera_capture_preview(camera, cameraFile, context);
            gp_file_get_data_and_size(cameraFile, data, size.getPointer());

            int dataSize = (int) size.getValue()
                                     .longValue();
            byte[] jpegBytes = data.getValue().getByteArray(0, dataSize);
            consumer.accept(jpegBytes);

            gp_file_free(cameraFile);
        }
    }
}
