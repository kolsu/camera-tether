package com.kolsuma.tether.gphoto2;

class GphotoException extends RuntimeException {

    GphotoException(PortResult result) {
        super(result.description + " Error code: " + result.code);
    }
}
