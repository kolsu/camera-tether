package com.kolsuma.tether.gphoto2;

public enum PortResult {

    GP_OK(0, "Everything is OK"),
    GP_ERROR_CORRUPTED_DATA(-102,"Corrupted data received"),
    GP_ERROR_FILE_EXISTS(-103, "File already exists"),
    GP_ERROR_MODEL_NOT_FOUND(-105, "Specified camera model was not found"),
    GP_ERROR_DIRECTORY_NOT_FOUND(-107, "Specified directory was not found"),
    GP_ERROR_FILE_NOT_FOUND(-108, "Specified file was not found"),
    GP_ERROR_DIRECTORY_EXISTS(-109, "Specified directory already exists"),
    GP_ERROR_CAMERA_BUSY(-110, "The camera is already busy"),
    GP_ERROR_PATH_NOT_ABSOLUTE(-111, "Path is not absolute"),
    GP_ERROR_CANCEL(-112, "Cancellation successful."),
    GP_ERROR_CAMERA_ERROR(-113, "Unspecified camera error"),
    GP_ERROR_OS_FAILURE(-114, "Unspecified failure of the operating system"),
    GP_ERROR(-1, "Generic Error"),
    GP_ERROR_BAD_PARAMETERS(-2, "Bad parameters passed"),
    GP_ERROR_NO_MEMORY(-3, "Out of memory"),
    GP_ERROR_LIBRARY(-4, "Error in the camera driver"),
    GP_ERROR_UNKNOWN_PORT(-5, "Unknown libgphoto2 port passed"),
    GP_ERROR_NOT_SUPPORTED(-6, "Functionality not supported"),
    GP_ERROR_IO(-7, "Generic I/O error"),
    GP_ERROR_FIXED_LIMIT_EXCEEDED(-8, "Buffer overflow of internal structure"),
    GP_ERROR_TIMEOUT(-10, "Operation timed out"),
    GP_ERROR_IO_SUPPORTED_SERIAL(-20, "Serial ports not supported"),
    GP_ERROR_IO_SUPPORTED_USB(-21, "USB ports not supported"),
    GP_ERROR_IO_INIT(-31, "Error initialising I/O"),
    GP_ERROR_IO_READ(-34, "I/O during read"),
    GP_ERROR_IO_WRITE(-35, "I/O during write"),
    GP_ERROR_IO_UPDATE(-37, "I/O during update of settings"),
    GP_ERROR_IO_SERIAL_SPEED(-41, "Specified serial speed not possible."),
    GP_ERROR_IO_USB_CLEAR_HALT(-51, "Error during USB Clear HALT"),
    GP_ERROR_IO_USB_FIND(-52, "Error when trying to find USB device"),
    GP_ERROR_IO_USB_CLAIM(-53, "Error when trying to claim the USB device"),
    GP_ERROR_IO_LOCK(-60, "Error when trying to lock the device"),
    GP_ERROR_HAL(-70, "Unspecified error when talking to HAL");

    int code;
    String description;

    PortResult(int code, String description) {
        this.code = code;
        this.description = description;
    }

    static PortResult[] index;

    static {
        index = new PortResult[200];
        for (PortResult result : PortResult.values()) {
            index[-result.code] = result;
        }
    }

    public static PortResult get(int code) {
        return index[-code];
    }

    @Override
    public String toString() {
        return "PortResult{" +
                "code=" + code +
                ", description='" + description + '\'' +
                '}';
    }
}
